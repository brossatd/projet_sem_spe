#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Audio.hpp>
#include <math.h>
#include <iostream>
#include <string>
#include <sstream>
using namespace sf;

int main()
{
    int positionX = 100;
    int positionY = 100;
    int positionMX = 500;
    int positionMY = 50;
    int rectPositionX = 580 ;
    int rectPositionY = 10 ;
    int x=100;
    int score=0;


    Music music;
    Font MyFont;
    Text text;
    Texture image1;
    Texture image2;
    Texture image3;
    Texture image4;

    SoundBuffer tir;
    Sound sound1;
    sound1.setBuffer(tir);
    sound1.setVolume(30.f);

    SoundBuffer dMonstre;
    Sound sound2;
    sound2.setBuffer(dMonstre);
    sound2.setVolume(50.f);

    if (!music.openFromFile("Jeu.ogg"))
        printf("PB de chargement de la musique!\n");
    if (!tir.loadFromFile("Gunshot1.mp3"))
        printf("PB de chargement du son!\n");
        if (!dMonstre.loadFromFile("monstre.wav"))
        printf("PB de chargement du son!\n");

    if (!image1.loadFromFile("shot.jpg"))
        printf("PB de chargement de l'image !\n");
    if (!image2.loadFromFile("perso.png"))
        printf("PB de chargement de l'image !\n");
    if (!image3.loadFromFile("Dragon.png"))
        printf("PB de chargement de l'image !\n");
    if (!image4.loadFromFile("DragonRed.png"))
        printf("PB de chargement de l'image !\n");
    if (!MyFont.loadFromFile("arial.ttf"))
        printf("Erreur du chargement de la police");

    music.setVolume(60.f);
    music.play();
    music.setLoop(true);


    Sprite shot;
    Sprite perso;
    Sprite monstre;
    Sprite monstreRed;

    shot.rotate(-90);
    shot.setPosition(positionX,positionY);
    shot.setTexture(image1);
    perso.setPosition(positionX,positionY);
    perso.setTexture(image2);
    monstre.setPosition(positionMX,positionMY);
    monstre.setTexture(image3);
    monstreRed.setPosition(positionMX,positionMY);
    monstreRed.setTexture(image4);

    RectangleShape rect(Vector2f(200, 20));
    rect.setPosition(rectPositionX,rectPositionY);
    rect.setFillColor(Color::Black);
    rect.setOutlineThickness(3);
    rect.setOutlineColor(Color::Red);

    std::ostringstream oss;
    oss << "Score : ";
    oss << score;
    std::string result = oss.str();

    text.setString(result);
    text.setCharacterSize(20);
    text.setFont(MyFont);
    text.setStyle(sf::Text::Bold);
    text.setColor(sf::Color::White);
    text.setPosition(rectPositionX+10,rectPositionY-2);

    RenderWindow window(VideoMode(800, 600), "SFML works!");
    window.draw(rect);
    window.draw(text);
    window.draw(perso);
    window.draw(monstre);
    window.display();

    while (window.isOpen())
    {
        Event event;
        while (window.pollEvent(event))
        {
            if (event.type == Event::MouseButtonPressed)
                if (event.mouseButton.button == Mouse::Left)
                {
                    sound1.play();
                    while(x!= positionMX)
                    {
                        x++;
                        std::ostringstream oss;
                        oss << "Score : ";
                        oss << score;
                        std::string result = oss.str();

                        text.setString(result);
                        text.setCharacterSize(20);
                        text.setFont(MyFont);
                        text.setStyle(sf::Text::Bold);
                        text.setColor(sf::Color::White);

                        shot.setPosition(x,positionY+15.5);

                        window.clear(Color::Black);
                        window.draw(perso);
                        window.draw(rect);
                        window.draw(text);
                        window.draw(monstre);
                        window.draw(shot);
                        window.display();
                    }
                    if(x >= positionMX)
                    {
                        sound2.play();
                        score=score+100;
                        window.clear(Color::Black);
                        std::ostringstream oss;
                        oss << "Score : ";
                        oss << score;
                        std::string result = oss.str();
                        text.setString(result);
                        text.setCharacterSize(20);
                        text.setFont(MyFont);
                        text.setStyle(sf::Text::Bold);
                        text.setColor(sf::Color::White);

                        window.draw(rect);
                        window.draw(text);
                        window.draw(perso);
                        window.draw(monstreRed);
                        window.display();
                        sleep(milliseconds(100));

                        window.clear(Color::Black);
                        window.draw(perso);
                        window.draw(rect);
                        window.draw(text);
                        window.draw(monstre);
                        window.display();
                        shot.setPosition(positionX,positionY);
                        x=100;
                    }
                }
        }

    }
    return EXIT_SUCCESS;
}
